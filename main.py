from utils import parse_message

if __name__ == '__main__':
    while True:
        line = raw_input("Enter message or -1 to exit:\n")
        if line == '-1':
            break
        print(parse_message(line))
