import json
import re
import requests
from bs4 import BeautifulSoup
from requests.exceptions import RequestException

REQUESTS_ERROR_CODE = 600
SUCCESSFUL_STATUS_CODES = [200, 201, 202, 203, 204, 205, 206]

# mention regex that find @ followed by one or more alpha numeric and preceded by
# beginning of line or not preceded by alpha numberic, dash, underscore and dot (as an email address)
MENTION_REGEX = re.compile(r"(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z0-9]+)")

# regex that finds custom emoticons defined as ( followed by 1 - 15 alphanumeric
# and ends with non-alphanumberic
EMOTICON_REGEX = re.compile(r"\(([A-Za-z0-9]{1,15})\)")

# regex to find http url.
URL_REGEX = re.compile(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")


def parse_message(message):
    """
    Parse a given message into its components. 3 supported components are links, emoticons and mentions
    :param message: a hip chat message
    :return: json string
    """
    mentions = parse_mentions(message)
    emoticons = parse_emoticons(message)
    links = parse_links(message)

    return json.dumps({"mentions": mentions, "emoticons": emoticons, "links": links})


def parse_links(message):
    """
    helper method that find & parse links in message. It returns an array of link
    dictionary which contains a original url and its title if the request succeeds
    If the html doesn't contain title tag, None will be returned for title
    If the html has empty title tag, empty string will be return
    If the request fails, error dict will be included in place of title
    it contains error code & human-friendly message. Error code are http status
    code if the connection was successful and the server return one. If not,
    600 is returned to indicate connection-related errors
    :param message:
    :return: Array of link dictionary
    """
    links =[]
    urls = URL_REGEX.findall(message)
    for url in urls:
        link = {'url': url}
        try:
            res = requests.get(url)
        except RequestException as ex:
            link['error'] = {"code": REQUESTS_ERROR_CODE, "message": str(ex.message)}
            links.append(link)
            continue

        if res.status_code in SUCCESSFUL_STATUS_CODES:
            soup = BeautifulSoup(res.content)
            # if content doesn't have title tag, soup.title return None,
            # if content has empty title tag, soup.title.string return None
            if soup.title:
                if soup.title.string:
                    link['title'] = soup.title.string
                else:
                    link['title'] = ''
            else:
                link['title'] = None
        else:
            link['error'] = {"code": res.status_code, "message": res.reason}

        links.append(link)
    return links


def parse_mentions(message):
    """
    Parse a message and return an array of mentioned users
    :param message:
    :return:
    """

    return MENTION_REGEX.findall(message)

def parse_emoticons(message):
    """
    Parse a message and return an array of custom emoticons
    :param message:
    :return:
    """
    return EMOTICON_REGEX.findall(message)

