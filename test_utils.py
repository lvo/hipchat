import unittest
import json
from requests.exceptions import RequestException
from mock import Mock, patch
from utils import parse_message, parse_links, parse_emoticons, parse_mentions


class TestMessageParser(unittest.TestCase):
    def setUp(self):
        self.error_url = 'http://error@test.com'
        self.valid_url = 'http://valid@test.com'
        self.not_found_url = 'http://not_found@test.com'
        self.correct_html_res = '<html lang="en" ><head> <meta content="abc"> ' \
                                '<title id ="title">hello</title> </head></html>'
        self.message_with_valid_link = "Olympics are starting soon: %s" % self.valid_url

    # Test parse_mentions function
    def test_non_alphanumeric_mention(self):
        message = "@$%^#&"
        mentions = parse_mentions(message)
        self.assertEqual(len(mentions), 0)

    def test_mention_stop_at_non_alphanumeric(self):
        message = "@chris123$^ how are you"
        mentions = parse_mentions(message)
        self.assertEqual(mentions[0], 'chris123')

    def test_email_instead_of_mention(self):
        message = "linhvo@yahoo.com"
        mentions = parse_mentions(message)
        self.assertEqual(len(mentions), 0)

    def test_mention_correct_user(self):
        message = "@chris @bob you around?"
        mentions = parse_mentions(message)

        self.assertEqual(mentions[0],'chris')
        self.assertEqual(mentions[1],'bob')
        self.assertEqual(len(mentions), 2)

    # Test parse_emoticons function
    def test_correct_emoticon(self):
        message = "Good morning! (megusta) (coffee)"
        emoticons = parse_emoticons(message)

        self.assertEqual(emoticons[0], 'megusta')
        self.assertEqual(emoticons[1], 'coffee')
        self.assertEqual(len(emoticons), 2)

    def test_not_emoticon_with_more_than_15_characters(self):
        message = "Hello friend (goodmorninghowareyoutoday)"
        emoticons = parse_emoticons(message)
        self.assertEqual(len(emoticons), 0)

    def test_not_emoticon_with_non_word(self):
        message = "Hello friend (hello 123)"
        emoticons = parse_emoticons(message)
        self.assertEqual(len(emoticons), 0)

    # Test parse_links function
    @patch('utils.requests')
    def test_correct_link(self, requests):
        for success_code in [200, 201, 202, 203]:
            response_mock = self._get_response_mock(requests, success_code)
            response_mock.content = self.correct_html_res
            links = parse_links(self.message_with_valid_link)

        self.assertEqual(links[0]['url'], "http://valid@test.com")
        self.assertEqual(links[0]['title'], "hello")

    @patch('utils.requests')
    def test_more_than_one_link(self, requests):
        message = "Test these links %s and %s" % (self.valid_url, self.not_found_url)
        valid_url_res = Mock()
        not_found_url_res = Mock()
        requests.get.side_effect = [valid_url_res, not_found_url_res]
        valid_url_res.status_code = 200
        valid_url_res.content = self.correct_html_res
        not_found_url_res.status_code = 404
        not_found_url_res.reason = 'Not found'
        links = parse_links(message)

        self.assertEqual(links[0]['url'], "http://valid@test.com")
        self.assertEqual(links[0]['title'], "hello")
        self.assertEqual(links[1]['url'], "http://not_found@test.com")
        self.assertEqual(links[1]['error']['code'], 404)
        self.assertEqual(links[1]['error']['message'], 'Not found')

    def _get_response_mock(self, requests, code):
        """
        helper to mock requests.get to return response mock with a given status code
        :param code:
        :param requests:
        :return:
        """
        response_mock = Mock()
        requests.get.return_value = response_mock
        response_mock.status_code = code
        return response_mock

    @patch('utils.requests')
    def test_link_return_error_status_code(self, requests):
        status_codes = {400: 'Bad request', 404: 'Not found', 500: 'Internal Server Error', 503: 'Service Unavailable'}
        for code, reason in status_codes.iteritems():
            response_mock = self._get_response_mock(requests, code)
            response_mock.reason = reason
            message = "@bob @john (success) such a cool feature; %s" % self.not_found_url
            links = parse_links(message)
            self.assertEqual(links[0]['error']['code'], code)
            self.assertEqual(links[0]['error']['message'], reason)

    @patch('utils.requests')
    def test_link_raise_exception(self, requests):
        exception_msg = 'foobar'
        requests.get.side_effect = RequestException(exception_msg)
        message = "@bob @john (success) such a cool feature; %s" % self.error_url
        links = parse_links(message)
        self.assertEqual(links[0]['error']['code'], 600)
        self.assertEqual(links[0]['error']['message'], exception_msg)

    @patch('utils.requests')
    def test_link_without_title(self, requests):
        response_mock = self._get_response_mock(requests, 200)
        response_mock.content = '<html><head>No Title Respones </head></html>'
        links = parse_links(self.message_with_valid_link)

        self.assertEqual(links[0]['url'], self.valid_url)
        self.assertEqual(links[0]['title'], None)

    @patch('utils.requests')
    def test_link_empty_title(self, requests):
        response_mock = self._get_response_mock(requests, 200)
        response_mock.content = '<html><head><title></title></head></html>'
        links = parse_links(self.message_with_valid_link)

        self.assertEqual(links[0]['url'], self.valid_url)
        self.assertEqual(links[0]['title'], '')

    @patch('utils.requests')
    def test_link_case_insensitive(self, requests):
        response_mock = self._get_response_mock(requests, 200)
        response_mock.content = '<html><head><TITLE>Hello</TITLE></head></HTML>'
        links = parse_links(self.message_with_valid_link)

        self.assertEqual(links[0]['url'], self.valid_url)
        self.assertEqual(links[0]['title'], 'Hello')

    @patch('utils.requests')
    def test_link_with_malformed_html(self, requests):
        response_mock = self._get_response_mock(requests, 200)
        response_mock.content = '<html><head><title>Hello'
        links = parse_links(self.message_with_valid_link)

        self.assertEqual(links[0]['url'], self.valid_url)
        self.assertEqual(links[0]['title'], 'Hello')

    # test parse_message function
    @patch('utils.parse_links')
    @patch('utils.parse_emoticons')
    @patch('utils.parse_mentions')
    def test_parse_message(self, mentions, emoticons, links):
        # test that parse_message call the other 3 methods
        mentions.return_value = ['bob', 'chris']
        emoticons.return_value = ['lol']
        links.return_value = [{'url': 'http://', 'title': 'title'}]
        message = 'test message'
        result = json.loads(parse_message(message))
        self.assertEqual(1, len(result['links']))
        self.assertEqual(1, len(result['emoticons']))
        self.assertEqual(2, len(result['mentions']))
        mentions.assert_called_once_with(message)
        emoticons.assert_called_once_with(message)
        links.assert_called_once_with(message)

    @patch('utils.parse_links')
    def test_parse_message_empty_array(self, links):
        links.return_value = []
        message = 'test message'
        result = json.loads(parse_message(message))
        self.assertIn('links', result)
        self.assertEqual(len(result['links']), 0)
        links.assert_called_once_with(message)

if __name__ == '__main__':
    unittest.main()