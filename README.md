HIPCHAT 
============

Overview
---

Takes a chat message string and returns a JSON string containing information about its contents

1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. 
1. Emoticons - consider 'custom' emoticons which are ASCII strings, no longer than 15 characters, contained in parenthesis.
1. Links - Any URLs contained in the message, along with the page's title.

Error Handling
---

1. If the message doesn't contain a particular component, empty array is returned in its place.
2. If the page corresponding to the url returns 4xx or 5xx status code, an error dictionary
 will be returned in place of the title. It'll include error code which is http code if one is
 available or 600 for connection errors and a human-friendly error message.
3. If the page html doesn't have title tag, null will be returned
4. If the page has an empty title tag, empty string will be returned

Setup
---
1. Install Python (optional)
    Mac OS X shipped with Python but it is usually a few version behind. I am using Python 2.7. Go to http://www.python.org/download/releases/ to find the latest 2.7 version
    Download and install the correct file
    NOTE: OSX Maverick ships with Python 2.7.5

1. Install Pip
1. Install the requirements for the project:

        pip install -r requirements.txt
        
        
Usage
---
The project shipped with a interactive console application that let you parse arbitrary message. Run by

        python main.py

Test
---
To run test:

        python -m unittest discover

Dependencies
---

Requests: Python library allows you to send HTTP/1.1 requests
BeautifulSoup: is a Python package for parsing HTML documents